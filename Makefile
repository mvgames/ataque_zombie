# Copyright 2017 Di Pietro M. and Purrello V.
#
# This file is part of Ataque Zombie.
#
# Ataque Zombie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ataque Zombie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ataque Zombie.  If not, see <http://www.gnu.org/licenses/>.
#

# Paths & Includes
OUTPUT_PATH		= .
SOURCES			= ./src
INCLUDES		= -I./include
MINGWINCLUDES		= -I./ncurses_mingw/include/ -I./ncurses_mingw/include/ncursesw
MINGWLIBS		= -L./ncurses_mingw/lib

# Flags
CXXFLAGS		= -std=c++11 -pedantic -Wall -O3
LDFLAGS			= -lncurses
STATICFLAGS		= -ltinfo -lgpm -static
MINGWFLAGS		= -DUSING_MINGW -lncursesw -static

# Parameters
questions		= yes
ENGLISH			= -DENGLISH
PARAMETERS		=

ifeq (${questions}, no)
	PARAMETERS += -DNO_QUESTIONS
endif

CXX			= g++
MINGW			= x86_64-w64-mingw32-g++
NAME			= ataque_zombie

# Rules
all: clean shared

shared:
	$(CXX) -o $(OUTPUT_PATH)/$(NAME)_shared $(SOURCES)/ataque_zombie.cpp $(INCLUDES) $(CXXFLAGS) $(LDFLAGS) $(PARAMETERS)

shared_en:
	$(CXX) -o $(OUTPUT_PATH)/$(NAME)_shared $(SOURCES)/ataque_zombie.cpp $(INCLUDES) $(CXXFLAGS) $(LDFLAGS) $(PARAMETERS) $(ENGLISH)

linux:
	$(CXX) -o $(OUTPUT_PATH)/$(NAME) $(SOURCES)/ataque_zombie.cpp $(INCLUDES) $(CXXFLAGS) $(LDFLAGS) $(STATICFLAGS) $(PARAMETERS)

linux_en:
	$(CXX) -o $(OUTPUT_PATH)/$(NAME) $(SOURCES)/ataque_zombie.cpp $(INCLUDES) $(CXXFLAGS) $(LDFLAGS) $(STATICFLAGS) $(PARAMETERS) $(ENGLISH)

windows:
	$(MINGW) -o $(OUTPUT_PATH)/$(NAME).exe $(SOURCES)/ataque_zombie.cpp $(INCLUDES) $(MINGWINCLUDES) $(MINGWLIBS) $(CXXFLAGS) $(MINGWFLAGS) $(PARAMETERS)

windows_en:
	$(MINGW) -o $(OUTPUT_PATH)/$(NAME).exe $(SOURCES)/ataque_zombie.cpp $(INCLUDES) $(MINGWINCLUDES) $(MINGWLIBS) $(CXXFLAGS) $(MINGWFLAGS) $(PARAMETERS) $(ENGLISH)

clean:
	rm -f $(NAME) $(NAME)_shared $(NAME).exe
