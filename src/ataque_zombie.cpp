/* Copyright 2017 Di Pietro M. and Purrello V.

 This file is part of Ataque Zombie.

 Ataque Zombie is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Ataque Zombie is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Ataque Zombie.  If not, see <http://www.gnu.org/licenses/>.
*/

// Min size to display the game
#define MIN_LINES 13
#define MIN_COLS 24

#ifdef ENGLISH
#include "lan_en.h"
#else
#include "lan_es.h"
#endif

#include "window.h"
#include "presentation.h"
#include "world.h"
#include <curses.h>

#ifndef USING_MINGW
#include <signal.h>
#endif

using namespace std;

static void finish (int sig);

int main (int argc, char* argv[])
{
    if (argc > 1) {
        cout << "ataque_zombie does not use any argument.\n\n";
        cout << "More info at https://gitlab.com/mvgames/ataque_zombie" << endl;
        exit(0);
    }

#ifndef USING_MINGW
    // arrange interrupts to terminate
    signal (SIGINT, finish);
    signal (SIGTERM, finish);
    signal (SIGHUP, finish);
    signal (SIGABRT, finish);
#endif

    initscr ();
    setlocale(LC_ALL, "");
    curs_set (0);
    while (LINES < MIN_LINES || COLS < MIN_COLS) {
        printw(STR_SMALL_WINDOW);
        refresh();
        getch ();
    }
    clear();
    presentation ();
    clear ();
    refresh ();
    char hero_char = select_character ();
    clear ();
    refresh ();
    int lvl = 1;
    int score = 0;
    int ch = 0;

    nonl(); // detect return key
    keypad(stdscr, TRUE); // map arrow keys (as in KEY_LEFT)
    cbreak (); // take input chars one at a time, no wait for \n
    noecho ();

    bool play = true;
    while (play) {
        World one (hero_char, lvl, score);
        one.print_map ();
        while (one.get_game_state () == GAMING) {
            ch = getch ();
            if (ch == 'q' || ch == 'Q')
                one.set_game_state (QUIT);
            else if (ch == 'h')
                one.show_help_msg ();
            else if (ch == 't')
                one.show_talk_msg ();
            else if (ch == ' ')
                one.set_bomb ();
            else if (ch == KEY_LEFT)
                one.move (-1, 0);
            else if (ch == KEY_RIGHT)
                one.move (1, 0);
            else if (ch == KEY_UP)
                one.move (0, -1);
            else if (ch == KEY_DOWN)
                one.move (0, 1);
            else
                one.move (0, 0);
            one.print_map ();
        }

        if (one.get_game_state () == LOST) {
            score = 0;
        }
        else if (one.get_game_state () == LVLUP) {
            one.show_centered_msg (STR_LEVEL_UP);
            getch ();
            score = one.get_score ();
            lvl += 1;
        }
        play = one.get_play_again();
        clear ();
        refresh ();
    }
    finish(0);
}

static void finish (int sig)
{
    endwin();
    cout << STR_GAME_OVER << endl;
    exit(sig);
}
