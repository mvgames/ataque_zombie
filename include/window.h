/* Copyright 2017 Di Pietro M. and Purrello V.

 This file is part of Ataque Zombie.

 Ataque Zombie is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Ataque Zombie is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Ataque Zombie.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _WINDOW_H
#define _WINDOW_H

#include <curses.h>

WINDOW *create_win (int height, int width, int startx, int starty)
{
    WINDOW *local_win;
    local_win = newwin (height, width, starty, startx);
    box (local_win, 0 , 0); // 0, 0 gives default characters for borders
    return local_win;
}

WINDOW *create_win_borderless (int height, int width, int startx, int starty)
{
    WINDOW *local_win;
    local_win = newwin (height, width, starty, startx);
    return local_win;
}

void destroy_win (WINDOW *local_win)
{
    delwin (local_win);
}

#endif /* _WINDOW_H */
