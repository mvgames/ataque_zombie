/* Copyright 2017 Di Pietro M. and Purrello V.

 This file is part of Ataque Zombie.

 Ataque Zombie is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Ataque Zombie is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Ataque Zombie.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _PRESENTATION_H
#define _PRESENTATION_H

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <curses.h>
#include <unistd.h>
#include "window.h"

using namespace std;

#define OFFSET 4

void presentation ()
{
    const vector<string> greeting = {
        "_______________ATAQUE ZOMBIE_______________",
        "                         by Victhor & Marik",
        "",
        "                              \\......\\",
        "                               C C  /",
        "                              /<   /",
        "               ___ __________/_#__/",
        "              /(- /(\\_\\________   \\",
        "              \\ ) \\ )_      \\o     \\",
        "              /|\\ /|\\       |'     |",
        "                            |     _|",
        "                            /o   __\\",
        "                           / '     |",
        "                          / /      |",
        "                         /_/\\______|",
        "                        (   _(    <",
        "                         \\    \\    \\",
        "                          \\    \\    |",
        "                           \\____\\____\\",
        "                           ____\\_\\__\\_\\",
        "                         /`   /`     o\\",
        "                         |___ |_______|",
    };

    int height = greeting.size() + 2;
    int width = 0;
    for (string line : greeting)
        if (width < (signed) line.length())
            width = (signed) line.length();
    width += 2;

    if (COLS < width || LINES < height) {
        printw ("____ATAQUE ZOMBIE____\n");
        printw ("   by Victhor & Marik\n\n");
        refresh ();
        sleep (2);
        return;
    }

    int startx = (COLS - width) / 2;
    int starty = (LINES - height) / 2;

    WINDOW *my_win;
    my_win = create_win (height, width, startx, starty);
    nodelay (my_win, TRUE);

    for (unsigned int i = 0; i < 3; ++i) {
        mvwprintw (my_win, i+1, 1, greeting[i].c_str());
    }
    wrefresh (my_win);
    sleep (1);

    float time_sleep = 300000;
    for (unsigned int i = 3; i < greeting.size(); ++i) {
        mvwprintw (my_win, i+1, 1, greeting[i].c_str());
        wrefresh (my_win);
        usleep (time_sleep);
        if (wgetch (my_win) != ERR)
            time_sleep = 0;
    }

    destroy_win (my_win);
    sleep(3);
}

char select_character ()
{
    WINDOW *my_win;

#ifndef NO_QUESTIONS
    bool skip = false; // Skip questions?

    WINDOW *head_win;
    const string header = STR_CHARACTER_EXAM;
    const int header_height = header.length()/COLS + 3;
    head_win = create_win(header_height, COLS, 0, 0);
    nodelay (head_win, TRUE);
    mvwprintw (head_win, 1, 1, header.c_str());
    wrefresh (head_win);
    sleep (3);

#else /* NO_QUESTIONS */
    const int header_height = OFFSET - 1;
#endif /* NO_QUESTIONS */

    int height = LINES - header_height - 2;
    int starty = header_height + 1;
    if (header_height < OFFSET) {
        height = LINES - OFFSET*2;
        starty = OFFSET;
    }

    int width = COLS - OFFSET*2;
    int startx = OFFSET;

    flushinp();
#ifndef NO_QUESTIONS
    if (! skip) {
        const vector<string> questions = {
            STR_QUESTION1,
            STR_QUESTION2,
            STR_QUESTION3,
            STR_QUESTION4,
            STR_QUESTION5,
            STR_QUESTION6,
            STR_QUESTION7,
        };

        for (string q : questions) {
            char answer[50];
            my_win = create_win(height, width, startx, starty);
            mvwprintw (my_win, 1, 1, q.c_str());
            wrefresh (my_win);
            mvwgetnstr (my_win, 3, 1, answer, 50);
            destroy_win (my_win);
        }

        my_win = create_win (height, width, startx, starty);
        mvwprintw (my_win, 2, 2, STR_ANSWER);
        wrefresh (my_win);
        sleep (3);
        destroy_win (my_win);
    }
    destroy_win (head_win);
#endif /* NO_QUESTIONS */

    srand ((unsigned) time(0));

    char hero_char[2] = {'O', 'A'};
    const string hero_name[2] = {"Osvaldo", "Amanda"};

    int r = rand()%2;

    std::string output;
    output.append(STR_YOUR_CHARACTER_IS);
    output.append(hero_name[r].c_str());
    my_win = create_win (height, width, startx, starty);
    mvwprintw (my_win, 2, 2, output.c_str());
    wrefresh (my_win);
    sleep (3);

    return hero_char[r];
}

#endif /* _PRESENTATION_H */
